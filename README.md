# Como configurar?

- altere o arquivo .env na raíz do projeto para sua configuração de banco

# 🏗 como criar e popular as tabelas do banco?

- execute o arquivo 01_create_tables.sql dentro do diretório src/db/seeds para criar o database e as tabelas.
- execute o arquivo 02_populate_db.sql dentro do diretório src/db/seeds para pupular as tabelas.

# como executar?

- abra o terminal na raíz do projeto e execute o comando **npm install**.
- execute o projeto com o comando **npm start**.

## Rotas:
- **GET** /login (Realiza login na aplicação).
- **POST** /create_user (cria um novo usuário, necessário estar logado com um usuário admin).
- **POST** /create_admin (cria um novo usuário admin, necessário estar logado com um usuário admin).
- **DELETE** /user/:id (exclusão lógica de um usuário, necessário estar logado com um usuário admin).
- **PUT** /user (edita um usuário, necessário estar logado com um usuário admin).

- **POST** /find_movies (cria um novo filme, necessário estar logado).
- **GET** /movie/:id (busca detalhes de um filme, necessário estar logado).
- **POST** /movie_create (busca detalhes de um filme, necessário estar logado como admin).

- **POST** /vote (cadastra um voto de avaliação no filme, necessário estar logado como usuário comun).
