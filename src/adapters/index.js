var express = require("express")
const userRoutes = require('./users')
const MovieRoutes = require('./movies')
const VotesRoutes = require('./votes')
const app = express();
const router = express.Router()

class Adapter {
    constructor() {
        this.userRoutes = new userRoutes()
        this.movieRoutes = new MovieRoutes()
        this.votesRoutes = new VotesRoutes()
    }
    start = () => {
        app.use(express.json());
        this.userRoutes.configRoutes(router);
        this.movieRoutes.configRoutes(router);
        this.votesRoutes.configRoutes(router);
        app.use(router);
        
        app.listen(8081, () => {
            console.log('server is running!');
        })
    }
}

module.exports = Adapter;

