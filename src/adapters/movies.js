const MovieControler = require('../controllers/movies')
const LoginMiddleware = require('../middleware/login')

class MovieAdapter {
    constructor() {
        this.movieControler = new MovieControler()
        this.loginMiddleware = new LoginMiddleware()
    }
    configRoutes = (router) => {
        router.post('/find_movies', this.loginMiddleware.baseLogin, async (req, res, next) => {
            try {
                const response = await this.movieControler.find(req.body.filter)
                res.status(200).send(response)
            } catch(error) {
                res.status(500).send(error)
            }
        })

        router.get('/movie/:id', this.loginMiddleware.baseLogin, async (req, res, next) => {
            try {
                const { id } = req.params
                const {status, body} = await this.movieControler.detailsMovie(id)
                res.status(status).send(body)
            } catch(error) {
                res.status(500).send(error)
            }
        })

        router.post('/movie_create', this.loginMiddleware.loginAdmin, async (req, res, next) => {
            try {
                const {status, body} = await this.movieControler.create(req.body)
                res.status(status).send(body)
            } catch(error) {
                res.status(500).send(error)
            }
        })
    }
}

module.exports = MovieAdapter;