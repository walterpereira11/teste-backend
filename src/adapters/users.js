const userController = require('../controllers/users')
const LoginMiddware = require('../middleware/login')

class UserAdapter {
    constructor() {
        this.userController = new userController()
        this.loginMiddware = new LoginMiddware()
    }

    createUser = async (user, res) => {
        const { status, body} = await this.userController.createUser(user)
        res.status(status).send(body)
    }
    configRoutes = (router) => {
        router.post('/login', async (req, res, next) => {
            try {
                const {status, body} = await this.userController.login({login: req.body.login, password: req.body.password})
                res.status(status).send(body)
                
            } catch(error) {
                res.status(500).send(error)

            }
        })

        router.post('/create_user', this.loginMiddware.loginAdmin, async (req, res) => {
            try {
                this.createUser({...req.body, admin: 0}, res)

            } catch(error) {
                res.status(500).send(error)

            }
        })

        router.post('/create_admin',this.loginMiddware.loginAdmin, async (req, res) => {
            try {
                this.createUser({...req.body, admin: 1}, res)
            
            } catch(error) {
                res.status(500).send(error)

            }
        })
    
        router.delete('/user/:id', this.loginMiddware.loginAdmin, async (req, res) => {
            try {
                const { id } = req.params
                const {status, body} = await this.userController.deleteUser(id)
                res.status(status).send(body)

            } catch(error) {
                res.status(500).send(error)
            }
        })
    
        router.put('/user', this.loginMiddware.loginAdmin, async (req, res) => {
            try {
                const {status, body} = await this.userController.updateUser(req.body)
                res.status(status).send(body)

            } catch(error) {
                res.status(500).send(error)
            }
        })
    }
}

module.exports = UserAdapter;