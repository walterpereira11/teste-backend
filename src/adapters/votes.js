const VoteController = require('../controllers/votes')
const LoginMiddleware = require('../middleware/login')

class VoteAdapter {
    constructor() {
        this.voteControler = new VoteController()
        this.loginMiddleware = new LoginMiddleware()
    }
    configRoutes = (router) => {
        router.post('/vote', this.loginMiddleware.loginUser, async (req, res, next) => {
            try {
                const response = await this.voteControler.create({...req.body, user_id: req.usuario.id})
                res.status(200).send(response)
                
            } catch(error) {
                res.status(500).send(error)
            }
        })
    }
}

module.exports = VoteAdapter;