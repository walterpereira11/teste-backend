const MovieService = require('../services/movies')

class MovieController {
    constructor() {
        this.movieService = new MovieService()
    }
    find = async (filter) => {
        return await this.movieService.find(filter)
    }

    detailsMovie = async (id) => {
        return await this.movieService.detailsMovie(id)
    }

    create = async (data) => {
        return await this.movieService.create(data)
    }
}

module.exports = MovieController