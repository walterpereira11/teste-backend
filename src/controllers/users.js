const UserService = require('../services/users')

class UserController {
    constructor() {
        this.userService = new UserService()
    }
    
    find = async (filter) => {
        return await this.userService.find(filter)
    }
    
    login = async (data) => {
        return await this.userService.login(data)
    }
    
    createUser = async (data) => {
        return await this.userService.createUser(data)
    }
    
    
    deleteUser = async (id) => {
        return await this.userService.deleteUser(id)
    }
    
    updateUser = async (data) => {
        return await this.userService.updateUser(data)
    }
}

module.exports = UserController;