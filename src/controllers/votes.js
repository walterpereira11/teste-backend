const VoteService = require('../services/votes')

class VoteController {
    constructor() {
        this.voteService = new VoteService()
    }
    create = async (data) => {
        return await this.voteService.create(data)
    }
}

module.exports = VoteController