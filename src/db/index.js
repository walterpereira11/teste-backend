const Sequelize = require('sequelize');
const User = require('../models/user')
const Movie = require('../models/movie')
const Actor = require('../models/actor')
const MovieActor = require('../models/movieActor')
const Category = require('../models/category')
const Vote = require('../models/vote')
const models = [
    {name: '_user', model: User},
    {name: '_actor', model: Actor},
    {name: '_movie_actor', model: MovieActor},
    {name: '_category', model: Category},
    {name: '_movie', model: Movie},
    {name: '_vote', model: Vote}
];

class Mysql {
    constructor() {
        this.driver = null
        this._connect()
        this._user = null
        this._movie = null
        this._actor = null
        this._movie_actor = null
        this._category = null
    }

    async _connect() {
        const {HOST, DB_PORT, DB_DEFAULT, DB_USERNAME, DB_PASSWORD, DB_DIALECT} = process.env

        this.driver = new Sequelize(DB_DEFAULT, DB_USERNAME, DB_PASSWORD != 0 ? DB_PASSWORD : null, {
            dialect: DB_DIALECT,
            host: HOST,
            port: DB_PORT,
            quoteIdentifiers: false,
            operatorsAliases: false
        })
        models.map(async ({name, model}) => {
            this[name] = await model.defineModel(this.driver)
        })
    }

}

module.exports = new Mysql()