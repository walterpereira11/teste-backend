CREATE DATABASE walter;

CREATE TABLE user (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  login varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  admin tinyint(1) NOT NULL,
  active tinyint(1) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE category (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE actor (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  age int(11) DEFAULT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE movie (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  director varchar(255) NOT NULL,
  abstract text NOT NULL,
  category_id int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE movie_actor (
  id int(11) NOT NULL AUTO_INCREMENT,
  movie_id int(11) NOT NULL,
  actor_id int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (movie_id) REFERENCES movie(id),
  FOREIGN KEY (actor_id) REFERENCES actor(id)
);

CREATE TABLE vote (
  id int(11) NOT NULL AUTO_INCREMENT,
  movie_id int(11) NOT NULL,
  user_id int(11) NOT NULL,
  registered_note int(11) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (movie_id) REFERENCES movie(id),
  FOREIGN KEY (user_id) REFERENCES user(id)
);
