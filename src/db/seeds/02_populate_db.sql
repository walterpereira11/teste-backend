INSERT INTO user (name, login, password, admin, active) VALUES
('Evandro Siqueira', 'evandro', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Ricardo Teixeira', 'ricardo', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Renan Fernandes', 'renan', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Luis Fernando', 'luis', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Jose Henrique', 'jose', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Pedro Silva', 'pedro', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Everton Pereira', 'everton', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Leandro Messias', 'leandro', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Laís Oliveira', 'lais', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Thais Castro', 'thais', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 1, 1),
('Marcela Gomes', 'marcela', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Everaldo Lucca', 'everaldo', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Odair Reinaldo', 'odair', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Marli Oliver', 'marli', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Renato Pasqual', 'renato', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Otavio Felizardo', 'otavio', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Renata Hermades', 'renata', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Tayana Siqueira', 'tayana', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Leyane Coelho', 'leyane', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1),
('Nathalia Martins', 'nathalia', '$2b$10$T5njUbCfWJZuRExic0/llO.KVTgMaiWdxcT4VllR53kwIaLaTSZdC', 0, 1);

INSERT INTO category (name) VALUES
('Ação'),
('Terror'),
('Suspense'),
('Romantico'),
('Comédia'),
('Aventura'),
('Anime'),
('Ficção'),
('Luta');

INSERT INTO movie (name, director, abstract, category_id) VALUES
    ('Spider-man: no way home', 'Jon Watts', 'O Homem-Aranha precisa lidar com as consequências da sua verdadeira identidade ter sido descoberta.', 1),
    ('Vennon 2', 'Andy Serkis', 'O relacionamento entre Eddie e Venom está evoluindo. Buscando a melhor forma de lidar com a inevitável simbiose, esse dois lados descobrem como viver juntos e, de alguma forma, se tornarem melhores juntos do que separados.', 1),
    ('Gente Grande', 'Dennis Dugan', 'A morte do treinador de basquete da infância de velhos amigos os reúne no mesmo lugar que celebraram um campeonato anos atrás. Os amigos, acompanhados de suas esposas e filhos, descobrem que idade não significa o mesmo que maturidade.', 5),
    ('Se eu fosse você 2', 'Daniel Filho', 'Cláudio e Helena estão prestes a se separar. Durante uma discussão, eles trocam de corpos mais uma vez e são obrigados a viver a vida do outro. Para complicar ainda mais a situação, a filha do casal está grávida e não sabe como contar aos pais.', 5),
    ('Jumandi 2', 'Jake Kasdan', 'Quatro adolescentes encontram um videogame cuja ação se passa em uma floresta tropical. Empolgados com o jogo, eles escolhem seus avatares para o desafio, mas um evento inesperado faz com que eles sejam transportados para dentro do universo fictício, transformando-os nos personagens da aventura.', 6),
    ('Kong: a ilha da caveira', 'Jordan Vogt-Roberts', 'Um ex-militar viaja com um grupo de desbravadores até a mítica Ilha da Caveira, onde seu irmão desapareceu enquanto procurava o Titan, soro que teria o poder de curar todas as doenças.', 6);

INSERT INTO actor (name, age) VALUES
    ('Dwayne Johnson', 38),
    ('Ton Holland', 24),
    ('Ton Hardy', 32),
    ('Woody Harrelson', 45),
    ('Samuel L. Jackson', 50),
    ('Tony Ramon', 48),
    ('Adan Sendler', 48);

INSERT INTO movie_actor(movie_id, actor_id) VALUES
    (1, 2),
    (2, 3),
    (2, 4),
    (3, 7),
    (4, 6),
    (5, 1),
    (6, 5);

INSERT INTO vote(movie_id, user_id, registered_note) VALUES
    (1, 11, 3),
    (1,12, 4),
    (1,13, 2),
    (1,14, 1),
    (1,15, 0),
    (1,16, 1),
    (1,17, 3),
    (1,18, 4),
    (1,19, 2),
    (1,20, 1),
    (2, 11, 3),
    (2,12, 3),
    (2,13, 1),
    (2,14, 1),
    (2,15, 2),
    (2,16, 3),
    (2,17, 2),
    (2,18, 0),
    (2,19, 4),
    (2,20, 2),
    (3,11, 2),
    (3,12, 1),
    (3,13, 3),
    (3,14, 4),
    (3,15, 2),
    (3,16, 1),
    (3,17, 4),
    (3,18, 4),
    (3,19, 0),
    (3,20, 3);

