const get_error = (status, message) => {
    console.log('opa :>> ');
    return {
        status,
        body: {
            message
        }
    }
}

const get_response = (status, data) => {
    return {
        status,
        body: {
            data
        }
    }
}
// RESPONSE_UNAUTHORIZED_LOGIN = {
//     status: 401,
//     body: {
//         message: 'Incorrect Password or Email!'
//     }
// }

// RESPONSE_UNAUTHORIZED = {
//     status: 401,
//     body: {
//         message: 'Unauthorized!'
//     }
// }

// EMAIL_EXISTS = {
//     status: 406,
//     body: {
//         message: 'Email already exists in system!'
//     }
// }

// LOGIN_OK = {
//     status: 200,
//     body: {
//         message: 'Login Succefull!'
//     }
// }

// DELETE_OK = {
//     status: 200,
//     body: {
//         message: 'Deleted Record!'
//     }
// }

// UPDATE_OK = {
//     status: 200,
//     body: {
//         message: 'Updated Record!'
//     }
// }
// INVALID_PROFILE = {
//     status: 401,
//     body: {
//         message: 'Invalid Profile!'
//     }
// }
// INVALID_PARAMETERS = {
//     status: 401,
//     body: {
//         message: 'Invalid Parameters!'
//     }
// }

// ERROR_PASSWORD_CONFIRMATION = {
//     status: 406,
//     body: {
//         message: "password different from confirmation password!"
//     }
// }

// CREATED = {
//     status: 200,
//     body: {
//         message: "Record Created!"
//     }
// }
// NOT_FOUND = {
//     status: 400,
//     body: {
//         message: "Record not found!"
//     }
// }

// ERROR = {
//     status: 500,
//     body: {
//         message: "A Error ocurrent!"
//     }
// }

// NO_RECORDS_CHANGED = {
//     status: 202,
//     body: {
//         message: "No Records Changed!"
//     }
// }

// MOVIE_NOT_FOUND = {
//     status: 400,
//     body: {
//         message: "Movie not found!"
//     }
// }

// INVALID_VOTE = {
//     status: 400,
//     body: {
//         message: "enter a note between 0 and 4!"
//     }
// }

module.exports = {
    get_error,
    get_response
    // RESPONSE_UNAUTHORIZED_LOGIN,
    // EMAIL_EXISTS,
    // LOGIN_OK,
    // CREATED,
    // UPDATE_OK,
    // INVALID_PROFILE,
    // ERROR_PASSWORD_CONFIRMATION,
    // RESPONSE_UNAUTHORIZED,
    // INVALID_PARAMETERS,
    // DELETE_OK,
    // NOT_FOUND,
    // ERROR,
    // NO_RECORDS_CHANGED,
    // MOVIE_NOT_FOUND,
    // INVALID_VOTE
}