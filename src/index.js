require('dotenv').config()
const Adapters = require('./adapters')

const adaptersInstance = new Adapters()

adaptersInstance.start();
