const jwt = require('jsonwebtoken')
const { RESPONSE_UNAUTHORIZED, INVALID_PROFILE, get_error } = require('../helpers/response_http')

class LoginMiddleware {

    baseLogin = (req, res, next) => {
        try {
            const [, token] = req.headers.authorization.split(' ')
            const decode = jwt.verify(token, process.env.JWT_KEY)
            req.usuario = decode
            next()
        } catch(e) {
            const error = get_error(200, 'Unauthorized!');
            return res.status(error.status).send(error.body)
        }
    }

    loginAdmin = (req, res, next) => {
        try {
            this.confirmPerfil(req, res, next, 1)
        } catch(e) {
            const error = get_error(401, e);
            return res.status(error.status).send(error.body)
        }
    }
    
    loginUser = (req, res, next) => {
        try {
            this.confirmPerfil(req, res, next, 0)
        } catch(e) {
            const error = get_error(401, e);
            return res.status(error.status).send(error.body)
        }
    }
    
    confirmPerfil = (req, res, next, type) => {
        const [, token] = req.headers.authorization.split(' ')
        const decode = jwt.verify(token, process.env.JWT_KEY)
        req.usuario = decode
        if(!this.isUserValido(decode, type)) {
            const {status, body} = get_error(401, 'Page authorized only to admin users.');
            res.status(status).send(body)
            return false;
        }
        return true;
    }

    isUserValido = (usuario, type) => {
        return usuario.admin == type;
    }  
}

module.exports = LoginMiddleware