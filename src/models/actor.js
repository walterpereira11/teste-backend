const Sequelize = require('sequelize');
const connection = require('../db');

class Actor extends Sequelize.Model {
    static async defineModel(driver) {
        const actors = driver.define('actor', {
            id: {
                type: Sequelize.INTEGER,
                required: 1,
                primaryKey: 1,
                autoIncrement: 1
            },
            name: {
                type: Sequelize.STRING,
                required: 1
            },
            age: {
                type: Sequelize.INTEGER,
                required: 1
            }
        }, {
            tableName: 'actor',
            freezeTableName: 0,
            timestamps: 0,
            createdAt: 0,
            updatedAt: 0
        })
    
        await actors.sync()
        return actors;
    }
}

module.exports = Actor;