const Sequelize = require('sequelize');
const connection = require('../db');

class Category extends Sequelize.Model {
    static async defineModel(driver) {
        const category = driver.define('category', {
            id: {
                type: Sequelize.INTEGER,
                required: 1,
                primaryKey: 1,
                autoIncrement: 1
            },
            name: {
                type: Sequelize.STRING,
                required: 1
            }
        }, {
            tableName: 'category',
            freezeTableName: 0,
            timestamps: 0,
            createdAt: 0,
            updatedAt: 0
        })
    
        await category.sync()
        return category;
    }
}

module.exports = Category;