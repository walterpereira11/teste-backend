const Sequelize = require('sequelize');

class Movie extends Sequelize.Model {
    static async defineModel(driver) {
        const movies = driver.define('movie', {
            id: {
                type: Sequelize.INTEGER,
                required: 1,
                primaryKey: 1,
                autoIncrement: 1
            },
            name: {
                type: Sequelize.STRING,
                required: 1
            },
            director: {
                type: Sequelize.STRING,
                required: 1
            },
            abstract: {
                type: Sequelize.STRING,
                required: 1
            },
            category_id: {
                type: Sequelize.INTEGER,
                required: 1
            }
        }, {
            tableName: 'movie',
            freezeTableName: 0,
            timestamps: 0,
            createdAt: 0,
            updatedAt: 0
        })

        await movies.sync()
        return movies;
    }
}

module.exports = Movie;