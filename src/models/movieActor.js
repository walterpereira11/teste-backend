const Sequelize = require('sequelize');
const connection = require('../db');

class Actor extends Sequelize.Model {
    static async defineModel(driver) {
        const movieActors = driver.define('movie_actor', {
            id: {
                type: Sequelize.INTEGER,
                required: 1,
                primaryKey: 1,
                autoIncrement: 1
            },
            movie_id: {
                type: Sequelize.INTEGER,
                required: 1
            },
            actor_id: {
                type: Sequelize.INTEGER,
                required: 1
            },
        }, {
            tableName: 'movie_actor',
            freezeTableName: 0,
            timestamps: 0,
            createdAt: 0,
            updatedAt: 0
        })
    
        await movieActors.sync()
        return movieActors;
    }
}

module.exports = Actor;