const Sequelize = require('sequelize');

class Vote extends Sequelize.Model {
    static async defineModel(driver) {
        const votes = driver.define('vote', {
            id: {
                type: Sequelize.INTEGER,
                required: 1,
                primaryKey: 1,
                autoIncrement: 1
            },
            movie_id: {
                type: Sequelize.INTEGER,
                required: 1
            },
            user_id: {
                type: Sequelize.INTEGER,
                required: 1
            },
            registered_note: {
                type: Sequelize.INTEGER,
                required: 1
            }
        }, {
            tableName: 'vote',
            freezeTableName: 0,
            timestamps: 0,
            createdAt: 0,
            updatedAt: 0
        })

        await votes.sync()
        return votes;
    }
}

module.exports = Vote;