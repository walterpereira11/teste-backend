const connection = require('../db')

class ActorRepository {

    findActorsByMovie = async (movieId) => {
        const response = await connection.driver.query(
            `   SELECT actor.* from actor
                JOIN movie_actor on movie_actor.actor_id = actor.id
                JOIN movie on movie.id = movie_actor.movie_id
                WHERE movie.id = :movieId`
            , {
            replacements: {movieId},
            model: connection._actor
        })

        return response.map(actor => actor.dataValues)
    }

    createMovieActor = async (movieActor) => {
        const {dataValues: newMovieActor} = await connection._movie_actor.create(movieActor)
        return newMovieActor;
    };
}

module.exports = ActorRepository