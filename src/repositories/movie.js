const connection = require('../db')

class MovieRepository {

    find = async (filter) => await connection._movie.findAll({ raw: true, where: filter});

    details = async (id) => {
        const [{dataValues: movie}] = await connection.driver.query(
            `   SELECT movie.*, category.name as category, ifnull(avg(registered_note), 0) as media 
                FROM movie 
                JOIN category on category.id = movie.category_id 
                RIGHT JOIN vote on vote.movie_id = movie.id
                WHERE movie.id = :id`
            , {
            replacements: {id},
            model: connection._movie
        })
        return movie
    };

    create = async (movie) => {
        const {dataValues: newMovie} = await connection._movie.create(movie)
        return newMovie;
    };
}

module.exports = MovieRepository