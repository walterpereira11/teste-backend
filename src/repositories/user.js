const connection = require('../db')

class UserRepository {
    find = async (filter) => await connection._user.findAll({ raw: true, where: filter});

    create = async (user) => {
        const {dataValues: newUser} = await connection._user.create(user)
        return newUser;
    };

    update = async (id, newUser) => {
        const [statusRequest] = await connection._user.update(newUser, {where: {id: id}})
        return statusRequest > 0;
    }

}

module.exports = UserRepository