const connection = require('../db')

class VoteRepository {

    create = async (note) => {
        const {dataValues: newNote} = await connection._vote.create(note)
        return newNote;
    };

    find = async (where) => {
        return await connection._vote.findAll({ raw: true, where})
    }
}

module.exports = VoteRepository