const MovieRepository = require('../repositories/movie')
const ActorRepository = require('../repositories/actor')
const { Op } = require('sequelize')
const { get_response, get_error } = require('../helpers/response_http')

class MovieService {
    constructor() {
        this.movieRepository = new MovieRepository()
        this.actorRepository = new ActorRepository()
    }
    find = async (filter) => {
        let where = {}
        if(filter.name) {
            where = {
                ...where,
                name: {
                    [Op.like]: `%${filter.name}%`
                }
            }
        }
        if(filter.category_id) {
            where = {...where, category_id: filter.category_id}
        }
        if(filter.director) {
            where = {
                ...where, 
                director: { 
                    [Op.like]: `%${filter.director}%`
                }
            }
        }
        const movies = await this.movieRepository.find(where) 
        return get_response(200, { movies })
    }

    detailsMovie = async(id) => {
        const movie = await this.movieRepository.details(id)
        const actors = await this.actorRepository.findActorsByMovie(id)
        if(movie) {
            const response = {
                details: {...movie, media: parseFloat(parseFloat(movie.media).toFixed(1))},
                actors
            }

            return get_response(200, response)
        }
        return get_error(404, "movie not found!")
    }

    create = async (data) => {
        if(!data.name || !data.director || !data.abstract || !data.category_id || !data.actors) return INVALID_PARAMETERS
        
        const movie = await this.movieRepository.create(data)
        data.actors.forEach(async (actor) => {
            await this.actorRepository.createMovieActor({movie_id: movie.id, actor_id: actor})
        });

        return get_response(200, {message: "Movie created!"})

    }
}

module.exports = MovieService