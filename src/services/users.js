const UserRepository = require('../repositories/user')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const {
    get_response,
    get_error
} = require('../helpers/response_http');

class UserService {
    constructor() {
        this.userRepository = new UserRepository()
    }
    find = async (filter) => {
        const response = await this.userRepository.find({...filter, active: 1})
        return response;
    }

    login = async ({login, password}) => {
        const response = await this.userRepository.find({login, active: 1})
        if(response.length > 0) {
            const [user] = response
    
            const authSuccess = await bcrypt.compare(password, user.password)
            if(authSuccess) {
                const token = jwt.sign({
                    id: user.id,
                    admin: user.admin
                },
                process.env.JWT_KEY,
                {
                    expiresIn: "1h"
                });
                return get_response(200, { message: 'Login Succefull!', token})
            }
        }
        return get_error(401, "Unauthorized!")
    }

    createUser = async(data) => {
        try {
            if(data.password !== data.confirmPassword) {
                return get_error(406, {message: 'password different from confirmation password!'})
            }
            const verification = await this.verifyLoginExists(data.login);
            if(verification) return verification
            const salt = await bcrypt.genSalt(10);
            data.password = await bcrypt.hash(data.password, salt);
            const newUser = await this.userRepository.create({...data, active: 1})
            if(newUser) {
                return get_response(200, {message: "Record Created!"})
            }
    
        } catch(error) {
            return get_error(500, error)
        }
        
    }

    deleteUser = async (id) => {
        try {
            const users = await this.userRepository.find({id, active: 1})
            if(users.length > 0) {
                let [userDelete] = users
                userDelete.active = 0
                const response = await this.userRepository.update(id, userDelete)
                if(response) {
                    return get_response(200, {message: 'User deleted!'})
                }
            } else {
                return get_error(404, "Not Found.")
            }
        } catch(error) {
            return get_error(500, error)
        }
    }

    updateUser = async ({newData, id}) => {
        try {
            if(newData.login) {
                const users = await this.userRepository.find({login: newData.login})
                if((users.length > 0) && users[0].id !== id) {
                    return get_error(400, "Login already exists.")
                }
            }
            const response = await this.userRepository.update(id, newData)
            return get_response(200, response ? "Updated!" : "No users changed")
        } catch(error) {
            return get_error(500, error)
        }
    
    }

    verifyLoginExists = async (login) => {
        const response = await this.userRepository.find({login})
        if(response.length > 0) {
            return get_error(400, "Login already exists.")
        }
        return null;
    }
}

module.exports = UserService