const VoteRepository = require('../repositories/vote')
const MovieRepository = require('../repositories/movie')
const {
    get_response,
    get_error
} = require('../helpers/response_http')

class VoteService {
    constructor() {
        this.voteRepository = new VoteRepository()
        this.movieRepository = new MovieRepository()
    }
    create = async({movie_id, user_id, registered_note}) => {
        try {
            if(!movie_id, !user_id, !registered_note || typeof registered_note !== 'number') return get_error(400, "Invalid Parameters!")
            if(registered_note < 0 || registered_note > 4) return get_error(400, "Invalid vote.")
            const [movie] = await this.movieRepository.find({id: movie_id})
            if(!movie) return get_error(400, "movie not found.")

            const [vote] = await this.voteRepository.find({movie_id, user_id})
            if(vote) {
                return get_error(400, "Vote already registered in movie.")
            }
            const newNote = await this.voteRepository.create({movie_id, user_id, registered_note})
            if(newNote) {
                return get_response(200, {message: 'Ok!'})
            }
        } catch(error) {
            return get_error(500, error)
        }

    }
}

module.exports = VoteService