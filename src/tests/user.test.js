require('dotenv').config()
const assert = require('assert')
const UserController = require('../controllers/users')
const Mysql = require('../db')

describe('Tests User', function() {
    let userController = null;
    this.timeout(Infinity)
    const MOCK_USER_UPDATE = {
        name: 'test user update',
        login: 'testUser' + new Date().getTime(),
        password: '123',
        confirmPassword: '123'
    }

    before(async () => {
        if(!Mysql._user) { // if the connection is not yet established wait 1 sec
            await new Promise(resolve => setTimeout(resolve, 3000))
        }
        userController = new UserController()
        await userController.createUser(MOCK_USER_UPDATE)
    })

    it('TEST 1 - login', async () => {
        const body = {
            login: 'normal user',
            password: '123'
        }
        const { token } = await userController.login(body)
        if(token !== null) {
            assert.ok(true)
        } else {
            assert.fail('error in execute login')
        }
    })

    it('TEST 2 - create user', async() => {
        const user = {
            name: 'user test',
            login: 'test' + new Date().getTime(),
            password: '123',
            confirmPassword: '123'
        }
        const {status} = await userController.createUser(user)
        assert.equal(status, 200);
    })

    it('TEST 3 - delete users', async() => {
        const [user] = await userController.find({name: 'user test'})
        const {status} = await userController.deleteUser(user.id)
        assert.equal(status, 200);
    })

    it('TEST 4 - update user', async() => {
        const [user] = await userController.find({login: MOCK_USER_UPDATE.login})
        const newName = 'updated test user'
        const { status } = await userController.updateUser({id: user.id, newData: {name: newName}})
        if(status == 200) {
            const [ userUpdated ] = await userController.find({login: MOCK_USER_UPDATE.login})
            assert.equal(userUpdated.id, user.id);
        } else {
            assert.fail('error in update!')
        }
    })
})
